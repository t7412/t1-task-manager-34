package ru.t1.chubarov.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IConnectionProvider;
import ru.t1.chubarov.tm.api.service.IServiceLocator;

import java.util.Properties;

public final class ConnectionProvider implements IConnectionProvider {

    @NotNull
    public static final String SERVER_PORT = "server.port";
    @NotNull
    public static final String SERVER_HOST = "server.host";
    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    private final IServiceLocator serviceLocator;

    public ConnectionProvider(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public String getHost() {
        return serviceLocator.getPropertyService().getServerHost();
    }

    @NotNull
    @Override
    public String getPort() {
        return serviceLocator.getPropertyService().getServerPortStr();
    }

}
