package ru.t1.chubarov.tm.exception.field;

public final class ExistsLoginException extends AbstractFieldException {

    public ExistsLoginException() {
        super("Error. Login is exists.");
    }

}
