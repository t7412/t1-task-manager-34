package ru.t1.chubarov.tm.exception.field;

public final class EmailExistsException extends AbstractFieldException {

    public EmailExistsException() {
        super("Error. Email is exists.");
    }

}
